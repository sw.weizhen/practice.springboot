package sw.dev.artifact.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sw.dev.artifact.orm.Wallet;
import sw.dev.artifact.repository.RepoWallet;

@Service
public class WalletServiceImpl implements WalletService {

	@Autowired
	private RepoWallet repoWallet;
	
	@Override
	public Optional<Wallet> findOneWalletById(Long id) {
		
		return repoWallet.findById(id);
	}

}
