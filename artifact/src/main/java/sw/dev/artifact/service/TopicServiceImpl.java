package sw.dev.artifact.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sw.dev.artifact.orm.Article;
import sw.dev.artifact.orm.Topic;
import sw.dev.artifact.repository.RepoTopic;

@Service
public class TopicServiceImpl implements TopicService {

	@Autowired
	private RepoTopic repoTopic;
	
	@Autowired
	private ArticleService articleServ;
	
	@Transactional
	@Override
	public Topic saveTopic(Topic topic) {
		return this.repoTopic.save(topic);
	}

	@Override
	public Topic updateTopic(Topic topic) {
		return this.repoTopic.save(topic);
	}

	@Override
	public Topic findTopicById(Long id) {
		
		Topic topic = this.repoTopic.findById(id).get();
		
		return topic;
	}

	@Override
	public Topic includeArticle(Long topicId, Long articleId) {
	
		Topic topic= this.repoTopic.findById(topicId).get();
		Article article = this.articleServ.findArticleById(articleId).get();
		
		topic.getListRefArticle().add(article);
		
		return this.repoTopic.save(topic);
	}

	@Override
	public Topic unincludeArticle(Long topicId, Long articleId) {
		
		Topic topic= this.repoTopic.findById(topicId).get();
		Article article = this.articleServ.findArticleById(articleId).get();
		
		topic.getListRefArticle().remove(article);
		
		return this.repoTopic.save(topic);
	}

	@Override
	public void deleteTopic(Long id) {
		this.repoTopic.deleteById(id);
	}

}
