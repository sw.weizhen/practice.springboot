package sw.dev.artifact.service;

import sw.dev.artifact.orm.Comment;

public interface CommentService {

	Comment saveComment(Long id, String comment);
	
	Comment updateComment(Long id, String comment);
	
	void deleteComment(Long id);
	
	void deleteCommentFromArticle(Long commentId);
}
