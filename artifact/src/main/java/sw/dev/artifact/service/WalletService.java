package sw.dev.artifact.service;

import java.util.Optional;

import sw.dev.artifact.orm.Wallet;

public interface WalletService {

	Optional<Wallet> findOneWalletById(Long id);
}
