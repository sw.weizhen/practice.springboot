package sw.dev.artifact.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sw.dev.artifact.orm.Article;
import sw.dev.artifact.orm.Comment;
import sw.dev.artifact.repository.RepoComment;

@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	private RepoComment repoComment;
	
	@Autowired
	private ArticleService articleServ;
	
	@Override
	public Comment saveComment(Long id, String comment) {
		
		Article optArticle = this.articleServ.findArticleById(id).get();
		
		Comment objComment = new Comment(comment);
		objComment.setRefArticle(optArticle);
		
		return repoComment.save(objComment);
	}

	@Override
	public void deleteComment(Long id) {
		this.repoComment.deleteById(id);
	}

	@Override
	public Comment updateComment(Long id, String comment) {
		
		Comment objComment = this.repoComment.findById(id).get();
		objComment.setContent(comment);
		
		return this.repoComment.save(objComment);
	}

	@Override
	public void deleteCommentFromArticle(Long commentId) {
		
		Comment objComment = this.repoComment.findById(commentId).get();
		objComment.clearComment();
		
		this.repoComment.deleteById(commentId);
	}

}
