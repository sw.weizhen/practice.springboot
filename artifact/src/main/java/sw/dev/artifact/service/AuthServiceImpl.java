package sw.dev.artifact.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sw.dev.artifact.orm.Author;
import sw.dev.artifact.orm.Wallet;
import sw.dev.artifact.repository.RepoAuthor;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private RepoAuthor repoAuth;
	
	
	@Override
	public Author saveAuthor(Author author, BigDecimal balance) {
		
		Wallet wallet = new Wallet(balance);
		
		author.setSignDate(new Date());
		author.setRefWallet(wallet);
		
		return repoAuth.save(author);
	}

	@Override
	public Author updateAuthor(Long id, String cellphone) {
		
		Author auth = repoAuth.findById(id).get();
		auth.setCellphone(cellphone);
		
		return repoAuth.save(auth);
	}

	@Override
	public Optional<Author> findOneAuthorById(Long id) {
		return repoAuth.findById(id);
	}

	@Override
	public void deleteAuthor(Long id) {
		this.repoAuth.deleteById(id);
		
	}

	@Override
	public Author updateBalance(Long id, BigDecimal balance) {
		
		Author auth = repoAuth.findById(id).get();
		auth.getRefWallet().setBalance(balance);
		
		return repoAuth.save(auth);
	}

}
