package sw.dev.artifact.service;

import java.util.Optional;

import sw.dev.artifact.orm.Article;

public interface ArticleService {

	Article saveArticle(Article article);
	
	Article updateArticle(Article article);
	
	Optional<Article> findArticleById(Long id);
	
	void deleteArticle(Long id);
}
