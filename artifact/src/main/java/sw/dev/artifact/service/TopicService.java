package sw.dev.artifact.service;


import sw.dev.artifact.orm.Topic;

public interface TopicService {
	
	Topic saveTopic(Topic topic);
	
	Topic updateTopic(Topic topic);
	
	Topic findTopicById(Long id);
	
	Topic includeArticle(Long topicId, Long articleId);
	
	Topic unincludeArticle(Long topicId, Long articleId);
	
	void deleteTopic(Long id);
	
}
