package sw.dev.artifact.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sw.dev.artifact.orm.Article;
import sw.dev.artifact.orm.Comment;
import sw.dev.artifact.repository.RepoArticle;

@Service
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	private RepoArticle repoArticle;
	
	@Override
	public Article saveArticle(Article article) {
		
		Comment comt1 = new Comment("Default comment 1");
		Comment comt2 = new Comment("Default comment 2");
		comt1.setRefArticle(article);
		comt2.setRefArticle(article);
		
		List<Comment> comments = new ArrayList<>();
		comments.add(comt2);
		comments.add(comt1);
		
		article.setRefComments(comments);
		
 		return repoArticle.save(article);
	}

	@Override
	public Article updateArticle(Article article) {
		
		return repoArticle.save(article);
	}

	@Override
	public Optional<Article> findArticleById(Long id) {
		return repoArticle.findById(id);
	}

	@Override
	public void deleteArticle(Long id) {
		repoArticle.deleteById(id);
	}

}
