package sw.dev.artifact.service;

import java.math.BigDecimal;
import java.util.Optional;

import sw.dev.artifact.orm.Author;

public interface AuthService {

	Author saveAuthor(Author author, BigDecimal balance);
	
	Author updateAuthor(Long id, String cellphone);

	Author updateBalance(Long id, BigDecimal balance);
	
	Optional<Author> findOneAuthorById(Long id);
	
	
	
	void deleteAuthor(Long id);
}
