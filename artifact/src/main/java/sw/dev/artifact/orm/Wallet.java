package sw.dev.artifact.orm;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Wallet {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private BigDecimal balance;
	
	@JsonIgnoreProperties({"refWallet"})
	@OneToOne(mappedBy = "refWallet", cascade = CascadeType.ALL)
	private Author refAuthor;
	
	public Wallet() {
		
	}
	
	public Wallet(BigDecimal balance) {
		this.balance = balance;
	}
	
	public void setRefAuthor(Author author) {
		this.refAuthor = author;
	}
	
	public Author getRefAuthor() {
		return this.refAuthor;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public BigDecimal getBalance() {
		return this.balance;
	}
}
