package sw.dev.artifact.orm;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Topic {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "listRefTopic"})
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			name = "topic_article_map",
			joinColumns = @JoinColumn(name = "topic_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "article_id", referencedColumnName = "id")
	)
	public List<Article> listRefArticle;
	
	public Topic() {
		
	}
	
	public Topic(String name) {
		this.name = name;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setListRefArticle(List<Article> listRefArticle) {
		this.listRefArticle = listRefArticle;
	}
	
	public List<Article> getListRefArticle() {
		
		return this.listRefArticle;
	}
}
