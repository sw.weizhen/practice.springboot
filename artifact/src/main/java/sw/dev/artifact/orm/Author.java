package sw.dev.artifact.orm;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "author")
public class Author {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "nickname", insertable = true)
	private String nickname;
	
	@Column(name = "cellphone")
	@Basic(fetch = FetchType.EAGER, optional = true) // optional = false -> ignoring this column
	private String cellphone;
	
//	@Temporal(TemporalType.DATE)
	@Column(name = "sign_date", 
			nullable = false,
			columnDefinition = "timestamp default current_timestamp")
	private Date signDate;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "refAuthor"})
	@JoinColumn(name = "author_wallet_id", referencedColumnName = "id")
	@OneToOne(	cascade = {CascadeType.ALL},
				optional = false,  
				fetch = FetchType.LAZY
			 )
	private Wallet refWallet;
	
	@Transient
	private String miscellaneous;
	
	public Author() {
	}
	
	public void setRefWallet(Wallet wallet) {
		this.refWallet = wallet;
	}
	
	public Wallet getRefWallet() {
		return this.refWallet;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public void setCellphone(String cellPhone) {
		this.cellphone = cellPhone;
	}
	
	public void setSignDate(Date signDate) {
		this.signDate = signDate;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public String getNickname() {
		return this.nickname;
	}
	
	public String getCellphone() {
		return this.cellphone;
	}
	
	public Date getSignDate() {
		return this.signDate;
	}
}
