package sw.dev.artifact.orm;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Article {

	@Id
	@GeneratedValue
	private Long id;
	private String title;
	private String content;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "listRefArticle"})
	@ManyToMany(mappedBy = "listRefArticle")
	private List<Topic> listRefTopic;
	
	//one side is maintained, default fetch = FetchType.LAZY
	@OneToMany(	mappedBy = "refArticle",
				cascade = {CascadeType.PERSIST, CascadeType.REMOVE}
	) // Default fetch = FetchType.LAZY
	@JsonIgnoreProperties({"refArticle"})
	private List<Comment> refComments;
	
	public Article() {
		
	}
	
	public void setRefComments(List<Comment> comments) {
		this.refComments = comments;
	}
	
	public List<Comment> getRefComments() {
		return this.refComments;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getContent() {
		return this.content;
	}
	
	public void setListRefTopic(List<Topic> listRefTopic) {
		this.listRefTopic = listRefTopic;
	}
	
	public List<Topic> getListRefTopic() {
		return this.listRefTopic;
	}
}
