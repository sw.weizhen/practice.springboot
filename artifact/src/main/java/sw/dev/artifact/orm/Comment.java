package sw.dev.artifact.orm;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Comment {

	@Id
	@GeneratedValue
	private Long id;
	private String content;

	// Many side is maintainer
	@ManyToOne
	@JsonIgnoreProperties({"refComments"})
	private Article refArticle;
	
	public void clearComment() {
		this.getRefArticle().getRefComments().remove(this);
	}
	
	public Comment() {
		
	}
	
	public Comment(String content) {
		this.content = content;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getContent() {
		return this.content;
	}
	
	public void setRefArticle(Article refArticle) {
		this.refArticle = refArticle;
	}
	
	public Article getRefArticle() {
		return this.refArticle;
	}
}
