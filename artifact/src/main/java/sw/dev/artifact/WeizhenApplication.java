package sw.dev.artifact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeizhenApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeizhenApplication.class, args);
	}

}
