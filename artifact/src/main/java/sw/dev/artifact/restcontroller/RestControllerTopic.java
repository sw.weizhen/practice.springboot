package sw.dev.artifact.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sw.dev.artifact.orm.Topic;
import sw.dev.artifact.service.TopicService;

@RestController
@RequestMapping("/topic")
public class RestControllerTopic {

	@Autowired
	private TopicService topicServ;
	
	@PostMapping("/opt")
	public Topic save(Topic topic) {
		return this.topicServ.saveTopic(topic);
	}
	
	@PutMapping("/opt")
	public Topic update(Topic topic) {
		return this.topicServ.updateTopic(topic);
	}
	
	@DeleteMapping("/opt/{id}")
	public void deleteTopic(@PathVariable("id") Long id) {
		this.topicServ.deleteTopic(id);
	}
	
	@PutMapping("/opt/{topicId}/{articleId}")
	public Topic includeArticle(@PathVariable("topicId")Long topicId, @PathVariable("articleId")Long articleId) {
		return this.topicServ.includeArticle(topicId, articleId);
	}

	@PostMapping("/opt/{topicId}/{articleId}")
	public Topic unincludeArticle(@PathVariable("topicId")Long topicId, @PathVariable("articleId")Long articleId) {
		return this.topicServ.unincludeArticle(topicId, articleId);
	}
	
	@GetMapping("/opt/{id}")
	public Topic findTopicById(@PathVariable("id")Long topicId) {
		
		Topic topic = this.topicServ.findTopicById(topicId);
		
		return topic;
	}
}








