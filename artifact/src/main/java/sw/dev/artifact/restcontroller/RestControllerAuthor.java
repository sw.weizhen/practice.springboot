package sw.dev.artifact.restcontroller;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import sw.dev.artifact.orm.Author;
import sw.dev.artifact.service.AuthService;

@RestController
@RequestMapping("/auth")
public class RestControllerAuthor {

	@Autowired
	private AuthService authServ;
	
	@PostMapping("/opt")
	public Author save(Author auth, @RequestParam("balance")BigDecimal balance) {
		return authServ.saveAuthor(auth, balance);
	}
	
	@GetMapping("/opt/{id}")
	public Optional<Author> findOneById(@PathVariable("id") Long id) {
		return authServ.findOneAuthorById(id);
	}
	
	@PutMapping("/opt/{id}")
	public Author update(@PathVariable("id")Long id, String cellphone) {
		return authServ.updateAuthor(id, cellphone);
	}
	
	@PutMapping("/opt/balance/{id}")
	public Author updateBalance(@PathVariable("id")Long id, @RequestParam("balance")BigDecimal balance) {
		return authServ.updateBalance(id, balance);
	}
	
	@DeleteMapping("/opt/{id}")
	public void deleteAuthor(@PathVariable("id")Long id) {
		authServ.deleteAuthor(id);
	}
}



















