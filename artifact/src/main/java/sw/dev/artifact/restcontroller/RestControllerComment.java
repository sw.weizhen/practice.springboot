package sw.dev.artifact.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import sw.dev.artifact.orm.Comment;
import sw.dev.artifact.service.CommentService;

@RestController
@RequestMapping("/comment")
public class RestControllerComment {

	@Autowired
	private CommentService commentServ;
	
	@PostMapping("/opt")
	public Comment save(@RequestParam("id")Long id, @RequestParam("comment")String comment) {
		return this.commentServ.saveComment(id, comment);
	}
	
	@PutMapping("/opt")
	public Comment update(@RequestParam("id")Long id, @RequestParam("comment")String comment) {
		return this.commentServ.updateComment(id, comment);
	}
	
	@DeleteMapping("/opt/{id}")
	public void deleteComment(@PathVariable("id")Long id) {
		this.commentServ.deleteComment(id);
	}
	
	@DeleteMapping("/opt/del/{id}")
	public void deleteCommentFromArticle(@PathVariable("id")Long id) {
		this.commentServ.deleteCommentFromArticle(id);
	}
	
	
}
