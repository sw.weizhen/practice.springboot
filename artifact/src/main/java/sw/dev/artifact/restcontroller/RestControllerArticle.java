package sw.dev.artifact.restcontroller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sw.dev.artifact.orm.Article;
import sw.dev.artifact.service.ArticleService;

@RestController
@RequestMapping("/article")
public class RestControllerArticle {

	@Autowired
	private ArticleService articleServ;
	
	@PostMapping("/opt")
	public Article save(Article article) {
		return articleServ.saveArticle(article);
	}
	
	@GetMapping("/opt/{id}")
	public Optional<Article> findOneById(@PathVariable("id") Long id) {
		return articleServ.findArticleById(id);
	}
	
	@DeleteMapping("/opt/{id}")
	public void deleteArticle(@PathVariable("id")Long id) {
		articleServ.deleteArticle(id);
	}
}
