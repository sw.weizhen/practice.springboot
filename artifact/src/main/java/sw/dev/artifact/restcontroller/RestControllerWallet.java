package sw.dev.artifact.restcontroller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sw.dev.artifact.orm.Wallet;
import sw.dev.artifact.service.WalletService;

@RestController
@RequestMapping("/wallet")
public class RestControllerWallet {

	@Autowired
	private WalletService walletServ;
	
	@GetMapping("/opt/{id}")
	public Optional<Wallet> findOneById(@PathVariable("id") Long id) {
		return walletServ.findOneWalletById(id);
	}
}
