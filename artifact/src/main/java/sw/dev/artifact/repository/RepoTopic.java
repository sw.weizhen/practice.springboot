package sw.dev.artifact.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sw.dev.artifact.orm.Topic;

public interface RepoTopic extends JpaRepository<Topic, Long> {

}
