package sw.dev.artifact.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sw.dev.artifact.orm.Author;

@Repository
public interface RepoAuthor extends JpaRepository<Author, Long>{

	
	/*
	 * JPQL not support "insert"
	 */
	
//	@Query("select auth from Author auth where auth.cellphone like %:phone%")
//	List<Author> defFindByPhone(@Param("phone")String phone);
	
	
}
