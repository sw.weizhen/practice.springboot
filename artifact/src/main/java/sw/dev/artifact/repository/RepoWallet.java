package sw.dev.artifact.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sw.dev.artifact.orm.Wallet;

public interface RepoWallet extends JpaRepository<Wallet, Long> {

}
