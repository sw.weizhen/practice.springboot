package sw.dev.artifact.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sw.dev.artifact.orm.Comment;

public interface RepoComment extends JpaRepository<Comment, Long>{

}
