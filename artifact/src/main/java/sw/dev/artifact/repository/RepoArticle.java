package sw.dev.artifact.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sw.dev.artifact.orm.Article;


public interface RepoArticle extends JpaRepository<Article, Long> {

}
